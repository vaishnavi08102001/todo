import { combineReducers } from "redux";
import {addNotesReducer} from "./NoteReducer"
 const appReducer=combineReducers({
    addNotesReducer,
 })
 const rootReducer=(state,action)=>{
    return appReducer(state,action)
 }
 export default rootReducer;