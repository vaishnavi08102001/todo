import { createStore } from "redux";
import rootReducer from ".";
export const configureStore=()=>{
    const store=createStore(
        rootReducer
    )
    return store;
}