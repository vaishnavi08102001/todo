
import { constants as types } from "../actionTypes";
const initialState={
    note:[]
}
const addNotesReducer=(state=initialState,action)=>{
switch(action.type){
    case types.ADD_NOTE:{
        console.log("show data from reducer",action.payload)
        return{
        note:[...state.note,action.payload]
        }
    }
    default:
        return state;
}
}
module.exports={
    addNotesReducer
}