// // App.js
// import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
// import FirstScreen from './Frontend/FirstScreen';
// import SecondScreen from './Frontend/SecondScreen';
// import Login from './Frontend/Login';
// import Registartion from './Frontend/SignUp';
// import Bottomtab from './Frontend/Bottomtab';
// import { View } from 'react-native';
// import SignUp from './Frontend/SignUp';
// import QuickNotes from './Frontend/QuickNotes';
// const Stack = createStackNavigator();

// function App() {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator>
//         <Stack.Screen name='FirstScreen' component={FirstScreen} options={{ headerShown: false }} />
//         <Stack.Screen name='SecondScreen' component={SecondScreen} options={{ headerShown: false }}/>
//         <Stack.Screen name='Login' component={Login} 
//             options={{
//               title:'',
//             }}
//             />
//             <Stack.Screen name='SignUp' component={SignUp}

//             options={{
//               title:'',
//             }}
//             />
//             <Stack.Screen name='Bottomtab' component={Bottomtab}options={{ headerShown: false }} /> 
//       </Stack.Navigator>
//        {/* <Bottomtab />  */}

//     </NavigationContainer>
//     // <Login />
//   );
// }

// export default App;
// App.js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import FirstScreen from './Frontend/FirstScreen';
import SecondScreen from './Frontend/SecondScreen';
import Login from './Frontend/Login';
import Registartion from './Frontend/SignUp';
import Bottomtab from './Frontend/Bottomtab';
import { View } from 'react-native';
import SignUp from './Frontend/SignUp';
import QuickNotes from './Frontend/QuickNotes';
import { Provider } from 'react-redux';
import { configureStore } from './redux/reducers/store';
import { LogBox } from 'react-native';
LogBox.ignoreAllLogs();
const Stack = createStackNavigator();

function App() {
  const store=configureStore();
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='FirstScreen' component={FirstScreen} options={{ headerShown: false }} />
        <Stack.Screen name='SecondScreen' component={SecondScreen} options={{ headerShown: false }}/>
        <Stack.Screen name='Login' component={Login} 
            options={{
              title:'',
            }}
            />
            <Stack.Screen name='SignUp' component={SignUp}

            options={{
              title:'',
            }}
            />
            <Stack.Screen name='Bottomtab' component={Bottomtab}options={{ headerShown: false }} /> 
      </Stack.Navigator>
       

    </NavigationContainer>
    </Provider>
    
  );
}

export default App;
