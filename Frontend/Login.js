import { View, Text, StyleSheet, TextInput, Touchable, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Login = () => {
const navigation = useNavigation();
const [email,setEmail]=useState('');
const [password,setPassword]=useState('');
const [badEmail,setBadEmail]=useState(false);
const [badPassword,setBadPassword]=useState(false);

const login=()=>{
if(email==''){
  setBadEmail(true);
}
else{
  setBadEmail(false);

if(password==''){
  setBadPassword(true)
}
else{
  setBadPassword(false)
  getData();
}
}}
const getData=async()=>{
  const mEmail=await AsyncStorage.getItem('EMAIL')
  const mPass=await AsyncStorage.getItem('PASSWORD')
  if(email===mEmail && mPass===password){
     await AsyncStorage.setItem("isLogin","true")

    navigation.navigate('Bottomtab')
  }
  else{
    alert('wrong password')
  }
}
  return (
    <View style={styles.conatiner}>
      <View style={styles.subContainer}>
      <Text style={styles.textHead}>Welcome back</Text>
      <Text style={styles.text}>Sign in to Continue</Text>
      

      <View style={styles.view2}>
      <Text style={styles.inputText}>Username</Text>
      <TextInput style={styles.textInput} placeholder='Enter your email' 
      value={email} onChangeText={(txt)=>{setEmail(txt)}}/>
      {badEmail===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter email id</Text>)}
      <Text style={[styles.inputText,{marginTop:30}]}>Password</Text>
      <TextInput style={styles.textInput} placeholder='Enter your password' 
      value={password} onChangeText={(txt)=>{setPassword(txt)}}/>
      {badPassword===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter Password</Text>)}
      <TouchableOpacity style={styles.signUp}
        onPress={()=>{
          navigation.navigate('SignUp')
        }} 
      >
        <Text style={styles.signupText}>Sign Up</Text>
      </TouchableOpacity>
      </View>
         
      <TouchableOpacity style={styles.button}
         onPress={()=>{
          login();
          
         }}>
      <Text style={styles.text2}>Log In</Text>
      </TouchableOpacity>

      </View>

    </View>
  )
}

export default Login
const styles = StyleSheet.create({
  conatiner:{
   flex:1,
   backgroundColor:'#fff',
   marginTop:0
  },
  subContainer:{
    height:'80%',
    marginTop:'15%',
  },
  textHead:{
    margin:20,
    fontSize:30,
    color:'black',
    opacity:1,
   
  },
  text:{
    fontSize:20,
    color:'black',
    marginLeft:20,
    opacity:0.5,
  },
  view2:{
    marginTop:30,
    padding:30,
  },
  inputText:{
    fontSize:24,
    color:'black',
    opacity:1,
    padding:5,
    
  },
  signupText:{
    fontSize:24,
    color:'black',
    opacity:1,
    padding:5,
    textDecorationLine:'underline'
  },
  textInput:{
    borderBottomWidth:1,
    opacity:0.5,
    fontSize:19,
  },
  signUp:{
    marginLeft:'70%',
    padding:5,

  },
  button:{
    backgroundColor:'#FF5050',
    padding:15,
    margin:25,
    borderRadius:5,
  },
  text2:{
    fontSize:24,
    color:'#fff',
    textAlign:'center'
  }
})


//C:\Users\Dell\Documents\keystore\keystore1.jks