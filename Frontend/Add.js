

// import { View, Text, Pressable, StyleSheet,TouchableOpacity } from 'react-native'
// import React from 'react'
// import { TextInput } from 'react-native-gesture-handler'
// import { useEffect } from 'react'
// import { useState } from 'react'
// import {openDatabase} from "react-native-sqlite-storage"
// // import { useNavigation } from '@react-navigation/native';
// import { UseDispatch, useDispatch } from 'react-redux'
// import SQLite from 'react-native-sqlite-storage';
// import {addNotes} from "../redux/actions"
// const db = SQLite.openDatabase(
//   { name: 'Vaishnavi.db', location: 'default' },
//   (db) => {
//     console.log("DataBasE ConNecteD");
//     db.executeSql(
//       'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, note TEXT)',
//       // 'ALTER TABLE notes ADD COLUMN color TEXT',
//       [],
//       () => {
//         console.log('Table "notes" created successfully');
//       },
//       (error) => {
//         console.error('Error creating table "notes":', error);
//       }
//     );
//   },
//   (error) => {
//     console.error('Error opening database:', error);
//   }
// );

// const colorOptions = ['#ff0000', '#00ff00', '#0000ff', '#ff00ff', '#00ffff'];
// const Add = ({navigation}) => {
//   const [selectedColor, setSelectedColor] = useState('black'); 
//   // const navigation = useNavigation();
//   const dispatch = useDispatch();
//   const [note, setNote] = useState('');
//   const handleColorChange = (color) => {
//     setSelectedColor(color);
//   };
//   const handleAddNotes = () => {
//      const notes={
//       id:Math.random()+10+Math.random(),
//       note:note,
//       color:selectedColor,
//      }
    
//     dispatch(addNotes(notes));
//     navigation.navigate("QuickNotes")
//     setSelectedColor(colorOptions[0]);
//   }
//   db.transaction(
//     (tx) => {
//       tx.executeSql(
//         'INSERT INTO notes (note,color) VALUES (?,?)',
//         [note,selectedColor],
//         (tx, results) => {
//           console.log('Note added successfully',results);
//         },
//         (error) => {
//           console.error(error);
//         }
//       );
//     },
//     (error) => {
//       console.error(error);
//     }
//   );
  
  

//    return (
//     <View  style={styles.Con1}>
//       <Text style={styles.Text}>Description</Text>
//       <TextInput placeholder='Enter your Notes here' multiline 
//          style={[styles.TextInput,{ borderColor: selectedColor, color: selectedColor}]}
//          value={note} onChangeText={(text) => setNote(text)}></TextInput>
//       <Text style={styles.Text}>Choose Color</Text>
//       <View style={styles.colorOptionsContainer}>
//         {colorOptions.map((color, index) => (
//           <TouchableOpacity
//             key={index}
//             onPress={() => handleColorChange(color)}
//             style={[styles.touchableOpacity , {backgroundColor:color}]}
//           />
//         ))}
//       </View>
//       <Pressable style={styles.Btn} onPress={handleAddNotes}>
//         <Text style={styles.Text2}>Done</Text>
//       </Pressable>
//     </View>
//   )
// }

// export default Add


// const styles = StyleSheet.create({
//   Con1: {
//     zIndex: 1,
//     marginTop: -40,
//     marginHorizontal: '7%',
//     padding: '5%',
//     backgroundColor: 'white',
//     elevation:5,
//     borderRadius:10
//   },
//   Text: {
//     color: 'black',
//     marginVertical: 18,
//     fontSize: 20
//   },
//   TextInput: {
//     height: 150,
//     textAlignVertical:"top",
//     multiline:true,
//     numberOfLines:50
//   },
//   Con2: {
//     width: '100%',
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   box1: {
//     backgroundColor: '#6074f9',
//     borderRadius:5,
//     height: 50,
//     width: 50
//   },
//   box2: {
//     backgroundColor: '#e42b6a',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   box3: {
//     backgroundColor: '#5abb56',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   box4: {
//     backgroundColor: '#3d3a62',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   box5: {
//     backgroundColor: '#f4ca8f',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   Btn: {
//     width: '90%',
//     height: 50,
//     justifyContent: 'center',
//     alignSelf: 'center',
//     backgroundColor: '#fb6464',
//     marginVertical: 40,
//     borderRadius: 5
//   },
//   Text2: {
//     color: 'white',
//     fontSize: 20,
//     alignSelf: 'center'
//   },
//   colorOptionsContainer: {
//     flexDirection: 'row',
//     justifyContent: 'center',
//     marginBottom: 10,
//   },
//   touchableOpacity:{ 
//     height: 50, 
//     width: 50, 
//     borderRadius: 10, 
//     margin: 5,

//   }

// }) 



import { View, Text, Pressable, StyleSheet,TouchableOpacity } from 'react-native'
import React from 'react'
import { TextInput } from 'react-native-gesture-handler'
import { useEffect } from 'react'
import { useState } from 'react'
import {openDatabase} from "react-native-sqlite-storage"
// import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux'
import SQLite from 'react-native-sqlite-storage';
import {addNotes} from "../redux/actions"
const db = SQLite.openDatabase(
  { name: 'Vaishnavi.db', location: 'default' },
  (db) => {
    console.log("DataBasE ConNecteD");
    db.executeSql(
      'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, note TEXT)',
      // 'ALTER TABLE notes ADD COLUMN color TEXT',
      [],
      () => {
        console.log('Table "notes" created successfully');
      },
      (error) => {
        console.error('Error creating table "notes":', error);
      }
    );
  },
  (error) => {
    console.error('Error opening database:', error);
  }
);

const colorOptions = ['#6074f9', '#e42b6a', '#5abb56', '#3d3a62', '#f4ca8f'];
const Add = ({navigation}) => {
  const [selectedColor, setSelectedColor] = useState('black'); 
  // const navigation = useNavigation();
  const dispatch = useDispatch();
  const [note, setNote] = useState('');
  const handleColorChange = (color) => {
    setSelectedColor(color);
  };
  const handleAddNotes = () => {
     const notes={
      id:Math.random()+10+Math.random(),
      note:note,
      color:selectedColor,
     }
    
    dispatch(addNotes(notes));
    // navigation.navigate("QuickNotes")
    // setSelectedColor(colorOptions[0]);
  
  db.transaction(
    (tx) => {
      tx.executeSql(
        'INSERT INTO notes (note,color) VALUES (?,?)',
        [notes.note,notes.color],
        (tx, results) => {
          console.log('Note added successfully',results);
        },
        (error) => {
          console.error(error);
        }
      );
    },
    (error) => {
      console.error(error);
    }
  );
  setNote('');
    setSelectedColor(colorOptions[0]);
    navigation.navigate('QuickNotes')
    
  }
  

   return (
    <View  style={styles.Con1}>
      <Text style={styles.Text}>Description</Text>
      <TextInput placeholder='Enter your Notes here' multiline={true} 
         style={[styles.TextInput,{ borderColor: selectedColor, color: selectedColor}]}
         value={note} onChangeText={(text) => setNote(text)}></TextInput>
      <Text style={styles.Text}>Choose Color</Text>
      <View style={styles.colorOptionsContainer}>
        {colorOptions.map((color, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => handleColorChange(color)}
            style={[styles.touchableOpacity , {backgroundColor:color}]}
          />
        ))}
      </View>
      <Pressable style={styles.Btn} onPress={handleAddNotes}>
        <Text style={styles.Text2}>Done</Text>
      </Pressable>
    </View>
  )
}

export default Add


const styles = StyleSheet.create({
  Con1: {
    zIndex: 1,
    marginTop: -40,
    marginHorizontal: '7%',
    padding: '5%',
    backgroundColor: 'white',
    elevation:5,
    borderRadius:10
  },
  Text: {
    color: 'black',
    marginVertical: 18,
    fontSize: 20
  },
  TextInput: {
    height: 150,
    textAlignVertical:"top",
    multiline:true,
    numberOfLines:50
  },
  Con2: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  box1: {
    backgroundColor: '#6074f9',
    borderRadius:5,
    height: 50,
    width: 50
  },
  box2: {
    backgroundColor: '#e42b6a',
    borderRadius: 5,
    height: 50,
    width: 50
  },
  box3: {
    backgroundColor: '#5abb56',
    borderRadius: 5,
    height: 50,
    width: 50
  },
  box4: {
    backgroundColor: '#3d3a62',
    borderRadius: 5,
    height: 50,
    width: 50
  },
  box5: {
    backgroundColor: '#f4ca8f',
    borderRadius: 5,
    height: 50,
    width: 50
  },
  Btn: {
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#fb6464',
    marginVertical: 40,
    borderRadius: 5
  },
  Text2: {
    color: 'white',
    fontSize: 20,
    alignSelf: 'center'
  },
  colorOptionsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10,
  },
  touchableOpacity:{ 
    height: 50, 
    width: 50, 
    borderRadius: 10, 
    margin: 5,

  }

}) 



