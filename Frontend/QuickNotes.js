// import { View, Text, FlatList, StyleSheet } from 'react-native'
// import { UseSelector, useSelector } from 'react-redux'
// import React,{useState,useEffect} from 'react'

// const QuickNotes = ({navigation}) => {
//   const data =useSelector((state)=>state.addNotesReducer.note)
//   console.log("show data from home page",data);
//   return (
//     <View>
//       <FlatList 
//       data={data}
//       keyExtractor={(item)=>item.id}
//       renderItem={(({item})=>{
//         return(
//           <View style={styles.noteContainer}>
//             <View style={{flex:0.5,marginTop:0}}>
//             <View style={[styles.colorLine, { borderBottomColor: item.color }]} />
//             <View style={{marginTop:15,}}>
//             <Text style={styles.text}>{item.note}</Text>
//             </View>
//             </View>
//            </View>
//         )
//       })}
//       />
      
//     </View>
//   )
// }
// const styles=StyleSheet.create({
// noteContainer:{
  
//   marginTop:15,
//   // alignItems:'center',
//   padding: 10,
//   elevation: 5,
//   shadowOffset: { width: 0, height: 2 },
//   shadowOpacity: 1,
//   shadowRadius: 8,
//   backgroundColor: 'white',
//   borderRadius: 15,
//   marginBottom: 5,
  
// },
// text:{
//   fontSize:25,
//   color:'black',
 
// },
// colorLine: {
//   width:150,
//   marginTop:0,
//   borderBottomWidth: 5,
// },
// })
// export default QuickNotes

import { View, Text, FlatList, StyleSheet ,Button} from 'react-native'
import {  useSelector } from 'react-redux'
import React,{useState,useEffect} from 'react'
import { useIsFocused } from '@react-navigation/native';
import SQLite from 'react-native-sqlite-storage';
const db = SQLite.openDatabase({ name: 'Vaishnavi.db', location: 'default' });
const QuickNotes = ({navigation}) => {
  // const data =useSelector((state)=>state.addNotesReducer.note)
  const isFocused = useIsFocused();
  const [notes, setNotes] = useState([]);
  useEffect(() => {
    if (isFocused) {
      fetchNotes();
    }
  }, [isFocused]);

  const fetchNotes = () => {
    db.transaction((tx) => {
      tx.executeSql('SELECT id, note, color FROM notes', [], (tx, results) => {
        const len = results.rows.length;
        const fetchedNotes = [];
        for (let i = 0; i < len; i++) {
          const row = results.rows.item(i);
          fetchedNotes.push(row);
        }
        setNotes(fetchedNotes);
      });
    });
  };

  // const deleteAllNotes = () => {
  //   db.transaction((tx) => {
  //     tx.executeSql('DELETE FROM notes', [], (_, results) => {
  //       fetchNotes();
  //     });
  //   });
  // };
  // console.log("show data from home page",data);
  return (
    <View>
      <FlatList 
      data={notes}
      keyExtractor={(item)=>item.id}
      renderItem={(({item})=>{
        return(
          <View style={styles.noteContainer}>
            <View style={{flex:0.5,marginTop:0}}>
            <View style={[styles.colorLine, { borderBottomColor: item.color }]} />
            <View style={{marginTop:15,}}>
            <Text style={styles.text}>{item.note}</Text>
            </View>
            </View>
           </View>
        )
      })}
      />
      {/* <Button title="Delete All Notes" onPress={deleteAllNotes} /> */}
    </View>
  )
}
const styles=StyleSheet.create({
noteContainer:{
  marginHorizontal:15,
  marginTop:15,
  
  padding: 10,
  elevation: 5,
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 1,
  shadowRadius: 8,
  backgroundColor: 'white',
  borderRadius: 9,
  marginBottom: 5,
  height:80,width:'90%'
},
text:{
  fontSize:25,
  color:'black',
 
},
colorLine: {
  width:150,
  marginTop:0,
  borderBottomWidth: 5,
},
})
export default QuickNotes

