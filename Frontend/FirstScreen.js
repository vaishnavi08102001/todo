// import { View, Text, Image, StyleSheet } from 'react-native'
// import React from 'react'

// const First =()=> {
//   return (
//     <View style={styles.container}>
//       <Image
//         source={require("./assets1/Page.png")}
//         style={styles.image} ></Image>
       
//         <Text style={styles.text}>aking</Text>
//     </View>
//   )
// }
// const styles = StyleSheet.create({
//     container:{
//      justifyContent:'center',
//       alignItems:'center',
//         flex:1,
        
        
//     },
//     image:{

//         width:200,
//         height:200,
//     },
//     text:{

//         fontSize:30,
//         padding:10,
//         opacity:1,
//         color:'black'
//     }
// })
// export default First
// FirstScreen.js
// FirstScreen.js
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const FirstScreen = ({ navigation }) => {
  useEffect(() => {
    const timeout = setTimeout(() => {
      Navigation();
    }, 3000);
    const Navigation=async()=>{
      
      const data= await AsyncStorage.getItem("isLogin")
      if(data=="true"){
        navigation.navigate('Bottomtab')
      }
      else{
        navigation.replace('SecondScreen');
      }
    }
    // Clear the timeout to avoid memory leaks
    return () => clearTimeout(timeout);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image  style={styles.image} source={require('../Images/Page1.png')} resizeMode='contain'/>
      <Text style={styles.text}>aking</Text>
    </View>
  );
};
const styles=StyleSheet.create({
  container:{
 flex:1,
 justifyContent:'center',
 alignItems:'center'
  },
  image:{
height:180,
width:180
  },
  text:{
opacity:1,
fontSize:30,
color:'black'
  }
})
export default FirstScreen;
