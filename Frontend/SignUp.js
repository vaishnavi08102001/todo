// import { View, Text, StyleSheet, TextInput, Touchable, TouchableOpacity } from 'react-native'
// import React from 'react'
// import  { useState } from 'react'
// import { useNavigation } from '@react-navigation/native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// const SignUp = () => {
//   const navigation = useNavigation();
//   const [email,setEmail]=useState('');
// const [password,setPassword]=useState('');
// const [confirmPassword,setConfirmPassword]=useState('');
// const [badEmail,setBadEmail]=useState(false);
// const [badPassword,setBadPassword]=useState(false);
// const [badConfirmPassword,setBadConfirmPassword]=useState(false);
// const[buttonDisabled,setButtonDisabled]=useState(false)
// const signupp=()=>{
//   setButtonDisabled(true)
//   if(email==''){
//     setBadEmail(true);
//     setButtonDisabled(true)
//     }
//   else{
//     setBadEmail(false);
//     if(password==''){
//       setBadPassword(true)
//       setButtonDisabled(true)
//       }
//     else{
//       setBadPassword(false)
//       if(confirmPassword==''){
//         setBadConfirmPassword(true)
//         setButtonDisabled(true)
//         }
//       else{
//         setBadConfirmPassword(false)
//         if(password!==confirmPassword){
//           setBadConfirmPassword(true)
//           setButtonDisabled(true)
//           }
//         else{
//           setBadConfirmPassword(false)
//           saveData();
//         }
//       }
//     }
//   }
// }
// const saveData=async()=>{
  
//     await AsyncStorage.setItem('EMAIL',email);
//     await AsyncStorage.setItem('PASSWORD',password);
//     console.log(":yes")
//     navigation.goBack();
  
// }
//   return (
//     <View style={styles.conatiner}>

//       <View style={styles.subContainer}>
//       <Text style={styles.textHead}>New User ?</Text>
//       <Text style={styles.text}>Sign Up to Continue</Text>
//       <View style={styles.view2}>
//       <Text style={styles.inputText}>Username</Text>
//       <TextInput style={styles.textInput} placeholder='Enter your email' 
//       value={email} onChangeText={(txt)=>{setEmail(txt)}}/>
//        {badEmail===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter email id</Text>)}
//       <Text style={[styles.inputText,{marginTop:30}]}>Password</Text>
//       <TextInput style={styles.textInput} placeholder='Enter your password' 
//       value={password} onChangeText={(txt)=>{setPassword(txt)}}/>
//       {badPassword===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter Password</Text>)}
//       <Text style={[styles.inputText,{marginTop:30}]}>Confirm Password</Text>
//       <TextInput style={styles.textInput} placeholder='Enter your password' 
//       value={confirmPassword} onChangeText={(txt)=>{setConfirmPassword(txt)}}/>
//       {badConfirmPassword===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter correct Password</Text>)}
//       </View>

//       <TouchableOpacity style={styles.button}
//       onPress={()=>{
//           signupp();
//             }} disabled={buttonDisabled}
//          >
        
//         <Text style={styles.text2}>Sign Up</Text>
//       </TouchableOpacity>
//       </View>

//     </View>
//   )
// }
// export default SignUp

// const styles = StyleSheet.create({
//   conatiner:{
//    flex:1,
//    backgroundColor:'#fff',
//    marginTop:0
//   },
//   subContainer:{
//     height:'80%',
//     marginTop:25,
//   },
//   textHead:{
//     margin:20,
//     fontSize:30,
//     color:'black',
//     opacity:1,
    
//   },
//   text:{
//     fontSize:20,
//     color:'black',
//     marginLeft:20,
//     opacity:0.5,
//   },
//   view2:{
//     marginTop:30,
//     padding:30,
//   },
//   inputText:{
//     fontSize:24,
//     color:'black',
//     opacity:1,
//   },
//   textInput:{
//     borderBottomWidth:1,
//     opacity:0.5,
//     fontSize:19,
    
//   },
//   signUp:{
//     marginLeft:'70%',
//     padding:5,
    
//   },
//   button:{
//     backgroundColor:'#FF5050',
//     padding:15,
//     margin:25,
//     borderRadius:20,
//   },
//   text2:{
//     fontSize:24,
//     color:'#fff',
//     textAlign:'center'
//   }
// })

import { View, Text, StyleSheet, TextInput, Touchable, TouchableOpacity, Alert } from 'react-native'
import React from 'react'
import  { useState } from 'react'
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const SignUp = () => {
  const navigation = useNavigation();
  const [email,setEmail]=useState('');
const [password,setPassword]=useState('');
const [confirmPassword,setConfirmPassword]=useState('');
const [badEmail,setBadEmail]=useState(false);
const [badPassword,setBadPassword]=useState(false);
const [badConfirmPassword,setBadConfirmPassword]=useState(false);
const[buttonDisabled,setButtonDisabled]=useState(false)
const signupp=()=>{
  setButtonDisabled(true)
  if(email==''){
    setBadEmail(true);
    setButtonDisabled(true)
    }
  else{
    setBadEmail(false);
    if(password==''){
      setBadPassword(true)
      setButtonDisabled(true)
      }
    else{
      setBadPassword(false)
      if(confirmPassword==''){
        setBadConfirmPassword(true)
        setButtonDisabled(true)
        }
      else{
        setBadConfirmPassword(false)
        if(password!==confirmPassword){
          setBadConfirmPassword(true)
          setButtonDisabled(false)
          }
        else{
          setBadConfirmPassword(false)
          if (!email.includes('@')) {
            setBadConfirmPassword(true)
            setButtonDisabled(true)
            Alert.alert('Invalid email format email should containe "@" ');
            setButtonDisabled(false)
          }
          else{
            setBadConfirmPassword(false)
            if (!(password.length >= 6 && /[0-9]/.test(password) && /[a-zA-Z]/.test(password))) {
              setBadConfirmPassword(true)
              setButtonDisabled(true)
              Alert.alert('Password must be 6 characters long and combination of no. and alphabets');
              setButtonDisabled(false)
            }
            else{
              setBadConfirmPassword(false)
          saveData();
            }
          }
          }
        }
      }
    }
  }

const saveData=async()=>{
  
    await AsyncStorage.setItem('EMAIL',email);
    await AsyncStorage.setItem('PASSWORD',password);
    console.log(":yes")
    navigation.goBack();
  
}
  return (
    <View style={styles.conatiner}>

      <View style={styles.subContainer}>
      <Text style={styles.textHead}>New User ?</Text>
      <Text style={styles.text}>Sign Up to Continue</Text>
      <View style={styles.view2}>
      <Text style={styles.inputText}>Username</Text>
      <TextInput style={styles.textInput} placeholder='Enter your email' 
      value={email} onChangeText={(txt)=>{setEmail(txt)}}/>
       {badEmail===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter email id</Text>)}
      <Text style={[styles.inputText,{marginTop:30}]}>Password</Text>
      <TextInput style={styles.textInput} placeholder='Enter your password' 
      value={password} onChangeText={(txt)=>{setPassword(txt)}}/>
      {badPassword===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter Password</Text>)}
      <Text style={[styles.inputText,{marginTop:30}]}>Confirm Password</Text>
      <TextInput style={styles.textInput} placeholder='Enter your password' 
      value={confirmPassword} onChangeText={(txt)=>{setConfirmPassword(txt)}}/>
      {badConfirmPassword===true &&(<Text style={{marginTop:10,marginLeft:30,color:'red',fontSize:18}}>Please Enter correct Password</Text>)}
      </View>

      <TouchableOpacity style={styles.button}
      onPress={()=>{
          signupp();
            }} disabled={buttonDisabled}
         >
        
        <Text style={styles.text2}>Sign Up</Text>
      </TouchableOpacity>
      </View>

    </View>
  )
}
export default SignUp

const styles = StyleSheet.create({
  conatiner:{
   flex:1,
   backgroundColor:'#fff',
   marginTop:0
  },
  subContainer:{
    height:'80%',
    marginTop:25,
  },
  textHead:{
    margin:20,
    fontSize:30,
    color:'black',
    opacity:1,
    
  },
  text:{
    fontSize:20,
    color:'black',
    marginLeft:20,
    opacity:0.5,
  },
  view2:{
    marginTop:30,
    padding:30,
  },
  inputText:{
    fontSize:24,
    color:'black',
    opacity:1,
  },
  textInput:{
    borderBottomWidth:1,
    opacity:0.5,
    fontSize:19,
    
  },
  signUp:{
    marginLeft:'70%',
    padding:5,
    
  },
  button:{
    backgroundColor:'#FF5050',
    padding:15,
    margin:25,
    borderRadius:5,
  },
  text2:{
    fontSize:24,
    color:'#fff',
    textAlign:'center'
  }
})
