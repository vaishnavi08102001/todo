// database.js
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('notes.db');

const createTable = () => {
  db.transaction((tx) => {
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT, color TEXT);',
      [],
      () => console.log('Table created successfully'),
      (_, error) => console.error('Error creating table: ', error)
    );
  });
};

const addNote = (description, color, callback) => {
  db.transaction((tx) => {
    tx.executeSql(
      'INSERT INTO notes (description, color) VALUES (?, ?);',
      [description, color],
      (_, result) => callback(result.insertId),
      (_, error) => console.error('Error adding note: ', error)
    );
  });
};

const getNotes = (callback) => {
  db.transaction((tx) => {
    tx.executeSql(
      'SELECT * FROM notes;',
      [],
      (_, result) => callback(result.rows._array),
      (_, error) => console.error('Error getting notes: ', error)
    );
  });
};

export { createTable, addNote, getNotes };
