import { View, Text, TouchableOpacity,StyleSheet } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import QuickNotes from './QuickNotes';
import MyTask from './MyTask';
import Menu from './Menu';
import Profile from './Profile';
import Add from './Add';
import React from 'react'
const Tab=createBottomTabNavigator();
const CustomTabBarButton = ({ children, onPress }) => (
  <TouchableOpacity style={styles.tabBarButton} onPress={onPress}>
    <View style={styles.redCircle}><View style={styles.Plus}>{children}</View></View>
  </TouchableOpacity>
);
const Bottomtab = () => {
  return (
    <Tab.Navigator 
   screenOptions={{
       tabBarStyle:{backgroundColor:'#242442',height:80},
       tabBarActiveTintColor: 'white', 
       tabBarInactiveTintColor: 'gray',
   }}
   tabBarOptions={{
    labelPosition: 'below-icon',
    labelStyle: {
      marginBottom: 10,
    fontSize:15,
    },
  }}
  >
      <Tab.Screen name='My Task' component={MyTask} 
      options={{
        tabBarLabel:"My Task",
        headerTitleAlign:'center',
        tabBarIcon:({focused,color,size})=>(
         < MaterialCommunityIcons name="check-circle-outline" color={color} size={35} />
        ),
      }}/>
      <Tab.Screen name='Menu' component={Menu}
      options={{
        tabBarLabel:"Menu",
        headerTitleAlign:'center',
        tabBarIcon:({focused,color,size})=>(
          <MaterialCommunityIcons name="microsoft" color={color} size={35} />
        ),
      }}/>
      <Tab.Screen name='Add' component={Add}
      options={{ 
        headerStyle:{
          backgroundColor:'#fb6464',
          height:150,
          
        },
        headerTintColor: 'white', 
          headerTitleAlign: 'center',
        title:'Add notes',
        
        tabBarLabel:'',
        tabBarIcon:({focused,color,size})=>(
          < MaterialCommunityIcons name="plus" color={'white'} size={25} />
        ),
        tabBarButton:(props)=>(
          <CustomTabBarButton {...props}/>
        )
        }}/>
      <Tab.Screen name='QuickNotes' component={QuickNotes} 
      options={{
        tabBarLabel:"Quick",
        headerTitleAlign:'center',
        tabBarIcon:({focused,color,size})=>(
         < MaterialCommunityIcons name="notebook-check" color={color} size={35} />
        ),
      }}/>
      <Tab.Screen name='Profile' component={Profile} 
      options={{
        tabBarLabel:"Profile",
        headerTitleAlign:'center',
        tabBarIcon:({focused,color,size})=>(
          <MaterialCommunityIcons name="account" color={color} size={35} />
        ),
      }}/>

    </Tab.Navigator>
  )
}
const styles = StyleSheet.create({
  tabBarButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  redCircle: {
    width: 60,
    height: 60,
    borderRadius: 35,
    backgroundColor: '#fb6464',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40, 
    
  },
  Plus: {
   
    bottom: -16, 
  },
});




export default Bottomtab